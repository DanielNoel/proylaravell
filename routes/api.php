<?php

use Illuminate\Http\Request;
use App\Empleado;

// MOSTRAR EMPLEADOS  
  Route::get('empleados',function(){
      $empleados=Empleado::get();
          return $empleados;
      
});
// RUTA PARA CREAR EMPLEADOS

  Route::post('empleados',function(Request $request){
   $request->validate([ 'nombres'=>'required|max:50',
    'apellidos'=>'required|max:50',
    'cedula'=>'required|max:20',
    'email'=>'required|max:50|email|unique:empleados',
    'lugar_nacimiento'=>'required|max:50',
    'sexo'=>'required|max:50',
    'estado_civil'=>'required|max:50',
    'telefono'=>'required|max:50']);
      $empleado=new Empleado;
      $empleado->nombres=$request->input('nombres');
      $empleado->apellidos=$request->input('apellidos');
      $empleado->cedula=$request->input('cedula');
      $empleado->email=$request->input('email');
      $empleado->lugar_nacimiento=$request->input('lugar_nacimiento');
      $empleado->sexo=$request->input('sexo');
      $empleado->estado_civil=$request->input('estado_civil');
      $empleado->telefono=$request->input('telefono');
      $empleado->save();
      return "Usuario Creado";
});
// OBTENER DATOS DE UN EMPLEADO por id
  Route::get('empleados/{id}',function($id){
     $empleado=Empleado::findOrFail($id);
     return $empleado;
});
Route::put('empleados/{id}',function(Request $request, $id){
   $request->validate([ 'nombres'=>'required|max:50',
    'apellidos'=>'required|max:50',
    'cedula'=>'required|max:20',
    'email'=>'required|max:50|email|unique:empleados,email,'.$id,
    'lugar_nacimiento'=>'required|max:50',
    'sexo'=>'required|max:50',
    'estado_civil'=>'required|max:50',
    'telefono'=>'required|max:50']);   
  $empleado= Empleado::findOrFail($id);
      $empleado->nombres=$request->input('nombres');
      $empleado->apellidos=$request->input('apellidos');
      $empleado->cedula=$request->input('cedula');
      $empleado->email=$request->input('email');
      $empleado->lugar_nacimiento=$request->input('lugar_nacimiento');
      $empleado->sexo=$request->input('sexo');
      $empleado->estado_civil=$request->input('estado_civil');
      $empleado->telefono=$request->input('telefono');
      $empleado->save();
      return "Empleado Actualizado";
});

Route::delete('empleados/{id}',function($id){
  $empleado=Empleado::findOrFail($id);
  // return $empleado; retorna la fila a eliminar
  $empleado->delete();
  return 'Empleado eliminado exitosamente';
});