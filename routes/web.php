<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
     return view('welcome');
 });

Route::resource('clientes', 'ClienteController');
Auth::routes();

// Route::get('/empleados', 'HomeController@index')->name('home');