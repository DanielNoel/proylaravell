<?php $__env->startSection('content'); ?>
    <div class="container">
        <div>
            <ul class="nav justify-content-center">
                <li @click="menu=0" class="nav-item">
                    <a class="nav-link active" href="#">Empleados</a>
                </li>
                <li @click="menu=1" class="nav-item">
                    <a class="nav-link" href="#">Cliente</a>
                </li>
                <li @click="menu=2" class="nav-item">
                    <a class="nav-link" href="#">Proveedor</a>
                </li>

            </ul>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        
                    </div>

                    <div class="card-body">
                        <template v-if="menu==0">
                             <empleado-s/>
                        </template>

                        <template v-if="menu==1">
                            <cliente-s/>
                           
                        </template>

                        <template v-if="menu==2">
                       <proveedore-s/>
                        </template>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\Proyectos\ProyLaravel\resources\views/welcome.blade.php ENDPATH**/ ?>