<?php

namespace App\Http\Controllers;

use App\Persons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           return DB::select("call getclientes('')");
          //return view('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  $request->validate([
        //  'nombres'=>'required|max:50',
        //  'dni'=>'required|max:8',
        //  'email'=>'required|max:50|email|unique:empleados',
        //  'telefono'=>'required|max:50']); 
        $firstname = trim($request->get("firstname"));
        $lastname = trim($request->get("lastname"));
        $dob = trim($request->get("dob"));
        $phone = trim($request->get("phone"));
        $email = trim($request->get("email"));
        $address = trim($request->get("address"));
        $transaction = trim($request->get("transaction"));
        $monto = trim($request->get("amount"));
        $date = trim($request->get("date"));
        DB::select("call insertarCliente (?,?,?,?,?,?,?,?,?)", array($firstname, $lastname, $dob, $phone, $email, $address, $transaction, $monto, $date));
        dd('se registro');

        return DB::select("call getclientes('$email')");  
     //return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Persons  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Persons $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Persons  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Persons $cliente)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Persons  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // $idpersonas = trim($request->get("idpersonas"));
        $id = trim($request->get("id"));
        $firstname = trim($request->get("firstname"));
        $lastname = trim($request->get("lastname"));
        $dob = trim($request->get("dob"));
        $phone = trim($request->get("phone"));
        $email = trim($request->get("email"));
        $address = trim($request->get("address"));
        $transaction = trim($request->get("transaction"));
        $monto = trim($request->get("amount"));
        $date = trim($request->get("date"));
         DB::select("call EditarCliente(?,?,?,?,?,?,?,?,?,?)", array( $id,$firstname, $lastname, $dob, $phone, $email, $address, $transaction, $monto, $date));
        // DB::statement("call EditarCliente ( $id , $firstname, $lastname, $dob, $phone, $email, $address, $transaction, $monto, $date) ");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Persons  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $id = trim($id);
        DB::select("call Eliminar(?)", array($id));
    }
}