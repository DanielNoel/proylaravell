window._ = require('lodash');

try {

    require('bootstrap');
} catch (e) {}


window.axios = require('axios');
window.toastr = require('toastr');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
